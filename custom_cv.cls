\LoadClass[12pt]{article}

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{custom_cv}[2019/21/01 My custom CV class]

\RequirePackage{titlesec}
\RequirePackage{xcolor}
\RequirePackage{tikz}
\usetikzlibrary{calc}
\RequirePackage[
    left=2.5cm, 
    right=2.5cm,
    top=2cm,
    bottom=2cm,
]{geometry}


\definecolor{bordercolour}{HTML}{258876}
\definecolor{backcolour}{HTML}{FFFFFF}
\definecolor{secondarycolour}{HTML}{1A5A71}

\pagecolor{backcolour}

\renewcommand{\maketitle}{%
    
    \begin{tikzpicture}[overlay, remember picture]

    \usetikzlibrary{calc}

    % Fill background with backcolour
    \fill[backcolour] (current page.north west) rectangle (current page.south east);

    % Draw triangles
    \fill[bordercolour] (current page.north west) -- (current page.north) -- (current page.west) -- cycle;
    \fill[bordercolour] (current page.south west) -- (current page.south) -- (current page.west) -- cycle;
    \fill[bordercolour] (current page.north east) -- (current page.north) -- (current page.east) -- cycle;
    \fill[bordercolour] (current page.south east) -- (current page.south) -- (current page.east) -- cycle;

    % Draw inner line
    \draw[secondarycolour, line width=1mm] 
        ($(current page.north)+(0, -1)$) -- 
        ($(current page.west)+(1, 0)$) -- 
        ($(current page.south)+(0, 1)$) --
        ($(current page.east)+(-1, 0)$) -- cycle;

    \end{tikzpicture}

    % Display text
    \vfill
    \begin{center}
        {\scshape
            {\Huge \@title \par}
            \vskip 2em
            {\LARGE \@author \par}
            \vskip 1.5em
            {\large \@date \par}
        }
    \end{center}
    \vfill

}

\newcommand{\fancyborder}{%

    \begin{tikzpicture}[overlay, remember picture]

    \fill[bordercolour]
        (current page.north west) -- ($(current page.north west)+(1, 0)$) 
        -- ($(current page.north west)+(0, -1)$) -- cycle;

    \fill[bordercolour]
        (current page.north east) -- ($(current page.north east)+(-1, 0)$) 
        -- ($(current page.north east)+(0, -1)$) -- cycle;

    \fill[bordercolour]
        (current page.south east) -- ($(current page.south east)+(-1, 0)$) 
        -- ($(current page.south east)+(0, 1)$) -- cycle;

    \fill[bordercolour]
        (current page.south west) -- ($(current page.south west)+(1, 0)$) 
        -- ($(current page.south west)+(0, 1)$) -- cycle;

    \draw[secondarycolour, line width=1mm]
        ($(current page.north west)+(0.5, -1.5)$) --
        ($(current page.north west)+(1.5, -0.5)$) --
        ($(current page.north east)+(-1.5, -0.5)$) --
        ($(current page.north east)+(-0.5, -1.5)$) --
        ($(current page.south east)+(-0.5, 1.5)$) --
        ($(current page.south east)+(-1.5, 0.5)$) --
        ($(current page.south west)+(1.5, 0.5)$) --
        ($(current page.south west)+(0.5, 1.5)$) --
        cycle;
        
    \end{tikzpicture}

}

\titleformat{\section}
    {\Large\scshape\raggedright}
    {}{0em}
    {}
    [\titlerule]

\titleformat{\subsection}
    {\large\scshape\raggedright}
    {}{0em}
    {}

\newcommand{\name}[1]{\centerline{\Huge{#1}}}

\newcommand{\contact}[4]{%
    \centerline{%
        \makebox[0pt][c]{%
            #1 {\large\textperiodcentered} #2 {\large\textperiodcentered} #3
            {\large\textperiodcentered} #4%
        }%
    }%
}

\newcommand{\datedsection}[2]{\section[#1]{#1 \hfill #2}}
\newcommand{\datedsubsection}[2]{\subsection[#1]{#1 \hfill #2}}

\newcommand{\projectitems}[3]{%
    \begin{itemize}
        \item #1
        \item #2
        \item #3
    \end{itemize}
}