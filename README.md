# latex-cv

A rather simple CV template class for LaTeX.

It is based on this guide by Overleaf
([Overleaf guide](https://www.overleaf.com/learn/latex/How_to_write_a_LaTeX_class_file_and_design_your_own_CV_(Part_1)))
.

The background graphics are drawn using the *tikz* package.